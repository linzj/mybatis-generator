package com.yh.edu.domain;

import java.util.Date;

/**************************************************
 * Copyright (c) 2016.
 * 文件名称: 
 * 摘　　要: 图书表
 * 作　　者: linzj
 * 完成日期: 2016-09-07
 **************************************************/
public class TbBookDemo {
    /**
     * 主键ID，无维护，直接数据库处理
     */
    private Integer bookId;

    /**
     * 图书名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 图书类型 1-新书 2-旧书
     */
    private Integer bookType;

    /**
     * 数据来自于哪个系统, all=移动联通相同, cmcc=移动, unicom=联通
     */
    private String fromDb;

    /**
     * 是否启用 1-启用 0-禁用 
     */
    private Integer isEnabled;

    /**
     * 是否删除 1-未删除 0-已删除 
     */
    private Integer isDeleted;

    private Date date1;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getBookType() {
        return bookType;
    }

    public void setBookType(Integer bookType) {
        this.bookType = bookType;
    }

    public String getFromDb() {
        return fromDb;
    }

    public void setFromDb(String fromDb) {
        this.fromDb = fromDb == null ? null : fromDb.trim();
    }

    public Integer getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Integer isEnabled) {
        this.isEnabled = isEnabled;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }
}