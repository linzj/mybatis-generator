package com.yh.edu.dao;

import com.yh.edu.domain.TbBookDemo;

public interface TbBookDemoDao {
    int delete(Integer bookId);

    int insert(TbBookDemo record);

    TbBookDemo selectById(Integer bookId);

    int update(TbBookDemo record);
}